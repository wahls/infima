% This script recreates the plot in Section VI of the paper
%
%   S. Wahls and H. Boche: "Lower Bounds on the Infima in Some H-infinity
%   Optimization Problems".
%
% Our configuration:
%
%   MATLAB:                     version 7.11
%   Control Systems Toolbox:    version 9.0
%   Robust Control Toolbox:     version 3.5

echo off;
clear all;

% Initialize some parameters

MaxN = 100;         % largest finite section size to be considered
q = 2;              % no of plant outputs
p1 = 1;             % no of external disturbances
p2 = 1;             % no of control inputs
n = 3;              % no of states

% Define the continuous-time plant given in Example 7.1 of
%
%   Y.S. Hung: "H-infinity Optimal Control -- Part 1. Model Matching", Int.
%   J. Control, vol. 49, no. 4, 1989.
%
% The infimum in the associated full information control problem is sqrt(5).

Ac = [0 1 0; 0 0 1; 0 -1 1];
Bc = [0 0; 1 0; 0 1];
Cc = [1 -1 1; 1 1 0];
Dc = [0 1; 1 0];
gam_opt = sqrt(5);

% Compute the infimum with MATLABs hinfsyn routine (LMI method)

P = ss(Ac,Bc,[Cc;eye(n);zeros(p1,n)],[Dc;zeros(n,p1+p2);eye(p1,p1+p2)]);
[K,CL,gam_lmi] = hinfsyn(P,n+p1,p2,'TOLGAM',eps*gam_opt,'METHOD','lmi');

% Replacing 'lmi' with 'ric' in the above call to hinfsyn is not possible
% because the assumptions of this method on the plant are not satisfied.
% The following alternative works,
%
%   P = ss(Ac,Bc,[Cc;eye(p1,n)],[Dc;eye(p1,p1+p2)]);
%   [K,CL,gam,INFO] = hinfsyn(P,p1,p2,'TOLGAM',eps*gam_opt,'METHOD','ric');
%   gam_ric = INFO.GAMFI;
%
% but using hinffi instead gives better results.

% Compute the infimum with MATLABs hinffi routine.

P = pck(Ac,Bc,Cc,Dc);
[K,G,gam_ric] = hinffi(P,p2,2,3,eps*gam_opt,-2);

% Use a bilinear transform to find a discrete-time version realization.
% This leaves the infimum unchanged.

[A,B,C,D] = bilinear(Ac,Bc,Cc,Dc,1);

% Display the transfer function of the resulting discrete-time plant.

[NUM,DEN] = ss2tf(A,B,C,D,1);
disp('M(1,1):');
disp(evalc('tf(6*NUM(1,:),6*DEN,-1)'));
disp('M(2,1):');
disp(evalc('tf(6*NUM(2,:),6*DEN,-1)'));

[NUM,DEN] = ss2tf(A,B,C,D,2);
disp('N(1,1):');
disp(evalc('tf(6*NUM(1,:),6*DEN,-1)'));
disp('N(2,1):');
disp(evalc('tf(6*NUM(2,:),6*DEN,-1)'));

% Compute the inverse outer factor of [M N; I 0] as described e.g. in
%
%   V. Ionescu and C. Oara: "Spectral and Inner-Outer Factorizations for
%   Discrete-Time Systems", IEEE Trans. Automat. Control, vol. 41, no. 12,
%   1996.

Cl = [C;zeros(p1,n)];
Dl = [D;eye(p1,p1+p2)];
[X,l,F] = dare(A,B,Cl'*Cl,Dl'*Dl,Cl'*Dl,eye(size(A)));
H = (Dl'*Dl + B'*X*B)^(1/2);
Oinv = ss(A-B*F,B*inv(H),-F,inv(H),-1);
O1plus = Oinv(1:p1,:);

% Display the transfer function of O1plus.

[NUM,DEN] = ss2tf(O1plus.A,O1plus.B,O1plus.C,O1plus.D,1);
disp('O1plus(1,1):');
disp(evalc('tf(NUM,DEN,-1)'));

[NUM,DEN] = ss2tf(O1plus.A,O1plus.B,O1plus.C,O1plus.D,2);
disp('O1plus(1,2):');
disp(evalc('tf(NUM,DEN,-1)'));

% Display the output controllability matrix and its rank

disp('Output controllability matrix M:');
M = [ ...
    O1plus.D; ...
    O1plus.C*O1plus.B; ...
    O1plus.C*O1plus.A*O1plus.B; ...
    O1plus.C*O1plus.A^2*O1plus.B ...
];
disp(M);

disp('Rank of M:');
disp(rank(M));

% Compute the lower bounds on the infimum given by the Theorems 2 and 5.

lower_bounds = zeros(1,MaxN);
GAM = O1plus.D;
new_row = [O1plus.C*O1plus.B O1plus.D];
for N=1:MaxN
    lower_bounds(N) = sqrt((1/min(svd(GAM)))^2-1);
    GAM = [GAM zeros(N*p1,p1+p2); new_row];
    new_row = [O1plus.C*O1plus.A^N*O1plus.B new_row];
end

% Compute the asymptotic error bound in Remark 2.

nrm = norm(O1plus,inf,eps);
cnd = nrm*gam_opt;
error_bound = eps*gam_opt*cnd/(1-eps*cnd);

% Plot the results.

figure;
semilogy( ...
    0:MaxN,abs(gam_opt-gam_lmi*ones(1,MaxN+1)),'-', ...
    0:MaxN,abs(gam_opt-gam_ric*ones(1,MaxN+1)),'-', ...
    1:MaxN,abs(gam_opt-lower_bounds),'-o', ...
    0:MaxN,error_bound*ones(1,MaxN+1),'--' ...
);
xlabel('N []');
ylabel('|exact infimum - computed approximation| []');
legend( ...
    'MATLAB (hinfsyn,LMI method)', ...
    'MATLAB (hinffi)', ...
    '1/sigma_{min}(Gamma_N)', ...
    'Asymptotic error bound in Remark 2' ...
);

% Uncomment the next line to save the current workspace in the file
% example.mat

%save('example.mat','-v6');